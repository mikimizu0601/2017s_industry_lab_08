package ictgradschool.industry.lab08.ex01;

import java.io.*;

public class ExerciseOne {

    public void start() {


        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {
        File file = new File("input2.txt");
        int numE = 0;
        int total = 0;

        try (FileReader fileReader = new FileReader(file)){
            int count =0;
            while(count!=-1){
                count = fileReader.read();
                 char letter = (char) count;
                if( letter == 'e' || letter == 'E'){
                    numE++;
                }
                total++;
            }

        } catch (IOException e) {
            System.out.println("Error");
        }


        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {
    File file = new File("input2.txt");
        int numE = 0;
        int total = 0;

        try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
//                line = reader.readLine();
                if (line == null) {
                    break;
                }
                numE +=  line.length() - line.replaceAll("e", "").replaceAll("E","").length();

                total = total + line.length();
            }
        }
         catch (IOException | NullPointerException e){
            e.printStackTrace();
            System.out.println("Error");
        }

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
