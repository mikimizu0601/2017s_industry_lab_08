package ictgradschool.industry.lab08.ex02;

import java.io.*;
import ictgradschool.Keyboard;

public class MyReader {

    public void start() {
        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();
        File file = new File (fileName);
        try(BufferedReader reader = new BufferedReader(new FileReader(file))){
        String line = null;
            while ((line = reader.readLine()) != null) {
            if (line == null){
                break;
            }
                System.out.print(line);
            }
        } catch(IOException e){
            System.out.println("Error" + e.getMessage());
        }
        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a BufferedReader.
    }

    public static void main(String[] args) {
        new MyReader().start();
    }
}
