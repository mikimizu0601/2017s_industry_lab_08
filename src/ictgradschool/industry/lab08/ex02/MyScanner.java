package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class MyScanner {

    public void start() {
        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();
        File file = new File (fileName);
        try(Scanner scanner=new Scanner(file)){
            while(scanner.hasNextLine()){
                System.out.println(scanner.nextLine());
            }
        }catch(IOException e){
            System.out.println("Error" + e.getMessage());
        }

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a Scanner.
    }

    public static void main(String[] args) {
        new MyScanner().start();
    }
}
