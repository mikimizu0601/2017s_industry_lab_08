package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;
import java.io.*;

/**
 * A simple program which should allow the user to type any number of text lines. The program will then
 * write them out to a file.
 */
public class MyWriter {

    public void start() {

        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();
        File file = new File (fileName);

        try(PrintWriter writer = new PrintWriter(new FileWriter(file))){
        // TODO Open a file for writing, using a PrintWriter.
//            writer.println();

        while (true) {

            System.out.print("Type a line of text, or just press ENTER to quit: ");
            String text = Keyboard.readInput();
            writer.println(text);

            if (text.isEmpty()) {
                break;
            }
        }
        } catch(IOException e){
            System.out.println("Error" + e.getMessage());
        }
            // TODO Write the user's line of text to a file.


        System.out.println("Done!");

    }

    public static void main(String[] args) {

        new MyWriter().start();

    }
}
